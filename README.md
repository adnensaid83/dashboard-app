# Next.js Dashboard Tutorial

voir site [dashboard app](https://dashboard-app-lilac.vercel.app/)
Email:user@nextmail.com
password:123456

Welcome to the Next.js Dashboard Tutorial! This comprehensive tutorial comprises 16 chapters, designed to guide you through the key features of Next.js, a powerful React framework for building modern web applications.

Unlike traditional tutorials that require you to write code from scratch, this course provides you with a head start by offering a pre-built codebase.

For more information, see the [course curriculum](https://nextjs.org/learn) on the Next.js Website.

# Getting Started

**Running the development server:** Run `npm i` to install the project's packages followed by `npm run dev` to start the development server..
